package loggerteam.jlogger.windows;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import loggerteam.jlogger.enumerations.E_Answers;
import loggerteam.jlogger.enumerations.E_MainFilters;
import loggerteam.jlogger.structures.S_ApplicationLog;
import loggerteam.jlogger.structures.S_MouseLog;
import loggerteam.jlogger.tools.T_Data;
import loggerteam.jlogger.tools.T_Logger;
import loggerteam.jlogger.tools.T_WindowsManager;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * Logika okna "LoggerComponent".
 */
public class L_LoggerComponentLogic
{
	// ========================================
	// POLA
	// ========================================
	
	/**
	 * Widok.
	 */
	private V_LoggerComponentView viewOfMainWindow;

	/**
	 * Model dla listy filtrów użytkownika.
	 */
	private DefaultListModel<String> userFiltersList;
	
	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public L_LoggerComponentLogic()
	{
		viewOfMainWindow = new V_LoggerComponentView();
		userFiltersList = new DefaultListModel<String>();
	}
	
	// ========================================
	// METODY
	// ========================================
	
	/**
	 * Inicjalizuje ramkę.
	 */
	public void initialize()
	{
		// Wyśrodkowanie ramki.
		viewOfMainWindow.setLocationRelativeTo(null);
		
		// Dodanie zdarzeń.
		addEvents();
		
		// Przypisanie modelów do list.
		viewOfMainWindow.lUserFilters.setModel(userFiltersList);
		viewOfMainWindow.lLogs.setModel(T_Logger.applicationLogsList);
		
		// Przypisanie filtru neutralnego.
		userFiltersList.add(0, "brak");
		viewOfMainWindow.lUserFilters.setModel(userFiltersList);
	}
	
	/**
	 * Wyświetla ramkę.
	 */
	public void show()
	{
		viewOfMainWindow.setVisible(true);
	}
	
	// ========================================
	// FUNKCJE
	// ========================================
	
	/**
	 * Dodaje zdarzenia od kontrolek.
	 */
	private void addEvents()
	{
		addEventsTo_frame();
		addEventsTo_miSaveLogsToTextFile();
		addEventsTo_miSaveLogsToBinaryFile();
		addEventsTo_miSaveLogsToXmlFile();
		addEventsTo_miFinish();
		addEventsTo_miAboutProgram();
		addEventsTo_tpTabs();
		addEventsTo_bSaveLogsToTextFile();
		addEventsTo_bSaveLogsToBinaryFile();
		addEventsTo_bSaveLogsToXmlFile();
		addEventsTo_bEraseLogs();
		addEventsTo_tfSearch();
		addEventsTo_bSearch();
		addEventsTo_cbMainFilter();
		addEventsTo_bAddUserFilter();
		addEventsTo_bEditUserFilter();
		addEventsTo_bDeleteUserFilter();
		addEventsTo_lUserFilters();
		addEventsTo_cbMouse();
		addEventsTo_cbKeyboard();
	}
	
	/**
	 * Zakańcza działanie programu.
	 */
	private void finishProgram()
	{
		E_Answers answer = T_WindowsManager.showQuestion("Czy zakończyc działanie programu?");
		if (answer == E_Answers.YES) System.exit(0);
	}
	
	private void generateLeftDiagram()
	{
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension dimension = toolkit.getScreenSize();
		int h = dimension.height;
		int w = dimension.width;
		
		int[] quarters = new int[4];
		
		for (S_MouseLog x : T_Data.mouseLogs)
		{
			if ((x.getxLocation() >= w / 2) && (x.getyLocation() < h / 2)) quarters[0]++;
			else if ((x.getxLocation() < w / 2) && (x.getyLocation() < h / 2)) quarters[1]++;
			else if ((x.getxLocation() < w / 2) && (x.getyLocation() >= h / 2)) quarters[2]++;
			else if ((x.getxLocation() >= w / 2) && (x.getyLocation() >= h / 2)) quarters[3]++;
		}
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.setValue(quarters[0], "I", "");
		dataset.setValue(quarters[1], "II", "");
		dataset.setValue(quarters[2], "III", "");
		dataset.setValue(quarters[3], "IV", "");
		JFreeChart barChart = ChartFactory.createBarChart(null, null, null, dataset, PlotOrientation.VERTICAL, true, false, false);
		viewOfMainWindow.lLeftDiagram.setIcon(new ImageIcon(barChart.createBufferedImage(200, 200)));
	}
	
	private void generateRightDiagram()
	{
		int[] types = new int[3];
		
		for (S_ApplicationLog x : T_Data.applicationLogs)
		{
			if (x.getFilter().getMainFilterType() == E_MainFilters.INFO) types[0]++;
			else if (x.getFilter().getMainFilterType() == E_MainFilters.WARNING) types[1]++;
			else if (x.getFilter().getMainFilterType() == E_MainFilters.ERROR) types[2]++;
		}
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.setValue(types[0], "INFO", "");
		dataset.setValue(types[1], "WARNING", "");
		dataset.setValue(types[2], "ERROR", "");
		JFreeChart barChart = ChartFactory.createBarChart(null, null, null, dataset, PlotOrientation.VERTICAL, true, false, false);
		viewOfMainWindow.lRightDiagram.setIcon(new ImageIcon(barChart.createBufferedImage(200, 200)));
	}
	
	private synchronized void searchResults(String value)
	{
		if (value.equals("")) 
		{
			viewOfMainWindow.lLogs.setModel(T_Logger.applicationLogsList);
			return;
		}
		
		T_Logger.filteredList.clear();
		char[] word = value.toLowerCase().toCharArray();
		
		for (S_ApplicationLog x : T_Data.applicationLogs)
		{
			String buffer = x.generateTextOfEvent();
			char[] text = buffer.toLowerCase().toCharArray();
			
			for (int i = 0; i < buffer.length(); i++)
			{
				for (int j = 0; j < value.length(); j++)
				{
					if (word[j] == text[i]) 
					{
						i++;
						if (j + 1 == value.length()) 
						{
							T_Logger.filteredList.add(0, x.generateTextOfEvent());
							i = buffer.length();
						}
					}
					else break;
				}
			}
		}
		
		viewOfMainWindow.lLogs.setModel(T_Logger.filteredList);
	}
	
	// ========================================
	// ZDARZENIA
	// ========================================
	
	public void addEventsTo_frame()
	{
		viewOfMainWindow.addWindowListener(new WindowListener()
		{			
			@Override
			public void windowOpened(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowIconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowClosing(WindowEvent arg0)
			{
				finishProgram();
			}
			
			@Override
			public void windowClosed(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowActivated(WindowEvent arg0)
			{
				;
			}
		});
	}
	
	public void addEventsTo_miSaveLogsToTextFile()
	{
		viewOfMainWindow.miSaveLogsToTextFile.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					JFileChooser chooser = new JFileChooser(); 
				    chooser.setCurrentDirectory(new java.io.File("."));
				    chooser.setDialogTitle("Wybierz katalog");
				    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    chooser.setAcceptAllFileFilterUsed(false);
				    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) T_Data.saveToTxtFile(chooser.getSelectedFile().getAbsolutePath());
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	public void addEventsTo_miSaveLogsToBinaryFile()
	{
		viewOfMainWindow.miSaveLogsToBinaryFile.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					JFileChooser chooser = new JFileChooser(); 
				    chooser.setCurrentDirectory(new java.io.File("."));
				    chooser.setDialogTitle("Wybierz katalog");
				    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    chooser.setAcceptAllFileFilterUsed(false);
				    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) T_Data.saveToBinaryFile(chooser.getSelectedFile().getAbsolutePath());
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	public void addEventsTo_miSaveLogsToXmlFile()
	{
		viewOfMainWindow.miSaveLogsToXmlFile.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				JFileChooser chooser = new JFileChooser(); 
			    chooser.setCurrentDirectory(new java.io.File("."));
			    chooser.setDialogTitle("Wybierz katalog");
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    chooser.setAcceptAllFileFilterUsed(false);
			    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
			    {
			    	try
					{
						T_Data.saveToXmlFile(chooser.getSelectedFile().getAbsolutePath());
					} 
			    	catch (IOException e)
					{
						e.printStackTrace();
					}
			    }
			}
		});
	}
	
	public void addEventsTo_miFinish()
	{
		viewOfMainWindow.miFinish.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				finishProgram();
			}
		});
	}
	
	public void addEventsTo_miAboutProgram()
	{
		viewOfMainWindow.miAboutProgram.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				T_WindowsManager.showAboutProgram();
			}
		});
	}
	
	public void addEventsTo_tpTabs()
	{
		viewOfMainWindow.tpTabs.addChangeListener(new ChangeListener()
		{
			@Override
			public synchronized void stateChanged(ChangeEvent arg0)
			{
				if (viewOfMainWindow.tpTabs.getSelectedIndex() == 2)
				{
					generateLeftDiagram();
					generateRightDiagram();
				}
			}
		});
	}
	
	public void addEventsTo_bSaveLogsToTextFile()
	{
		viewOfMainWindow.bSaveLogsToTextFile.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					JFileChooser chooser = new JFileChooser(); 
				    chooser.setCurrentDirectory(new java.io.File("."));
				    chooser.setDialogTitle("Wybierz katalog");
				    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    chooser.setAcceptAllFileFilterUsed(false);
				    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) T_Data.saveToTxtFile(chooser.getSelectedFile().getAbsolutePath());
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	public void addEventsTo_bSaveLogsToBinaryFile()
	{
		viewOfMainWindow.bSaveLogsToBinaryFile.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				try
				{
					JFileChooser chooser = new JFileChooser(); 
				    chooser.setCurrentDirectory(new java.io.File("."));
				    chooser.setDialogTitle("Wybierz katalog");
				    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    chooser.setAcceptAllFileFilterUsed(false);
				    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) T_Data.saveToBinaryFile(chooser.getSelectedFile().getAbsolutePath());
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	public void addEventsTo_bSaveLogsToXmlFile()
	{
		viewOfMainWindow.bSaveLogsToXmlFile.addActionListener(new ActionListener()
		{		
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				JFileChooser chooser = new JFileChooser(); 
			    chooser.setCurrentDirectory(new java.io.File("."));
			    chooser.setDialogTitle("Wybierz katalog");
			    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			    chooser.setAcceptAllFileFilterUsed(false);
			    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
			    {
			    	try
					{
						T_Data.saveToXmlFile(chooser.getSelectedFile().getAbsolutePath());
					} 
			    	catch (IOException e)
					{
						e.printStackTrace();
					}
			    }
			}
		});
	}
	
	public void addEventsTo_bEraseLogs()
	{
		viewOfMainWindow.bEraseLogs.addActionListener(new ActionListener()
		{		
			@Override
			public synchronized void actionPerformed(ActionEvent arg0)
			{
				T_Logger.applicationLogsList.clear();
				T_Logger.eventsList.clear();
				T_Logger.filteredList.clear();
				
				T_Data.applicationLogs.clear();
				T_Data.mouseLogs.clear();
				T_Data.keyboardLogs.clear();
			}
		});
	}
	
	public void addEventsTo_tfSearch()
	{
		viewOfMainWindow.tfSearch.addFocusListener(new FocusListener()
		{
			@Override
			public void focusLost(FocusEvent arg0)
			{
				if (viewOfMainWindow.tfSearch.getText().isEmpty()) viewOfMainWindow.tfSearch.setText("Wyszukaj...");
			}
			
			@Override
			public void focusGained(FocusEvent arg0)
			{
				if (viewOfMainWindow.tfSearch.getText().equals("Wyszukaj...")) viewOfMainWindow.tfSearch.setText("");
			}
		});
		
		viewOfMainWindow.tfSearch.addKeyListener(new KeyListener()
		{
			@Override
			public void keyTyped(KeyEvent arg0)
			{
				;
			}
			
			@Override
			public void keyReleased(KeyEvent arg0)
			{
				;
			}
			
			@Override
			public void keyPressed(KeyEvent arg0)
			{
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) searchResults(viewOfMainWindow.tfSearch.getText());
			}
		});
	}
	
	public void addEventsTo_bSearch()
	{
		viewOfMainWindow.bSearch.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				searchResults(viewOfMainWindow.tfSearch.getText());
			}
		});
	}
	
	public void addEventsTo_cbMainFilter()
	{
		viewOfMainWindow.cbMainFilter.addActionListener(new ActionListener()
		{		
			@Override
			public synchronized void actionPerformed(ActionEvent arg0)
			{
				T_Logger.filteredList.clear();
				
				switch (viewOfMainWindow.cbMainFilter.getSelectedIndex())
				{
					case 0:
					{
						viewOfMainWindow.lLogs.setModel(T_Logger.applicationLogsList);
						break;
					}
						
					case 1:
					{
						for (S_ApplicationLog x : T_Data.applicationLogs)
						{
							if (x.getFilter().getMainFilterType() == E_MainFilters.INFO) T_Logger.filteredList.add(0, x.generateTextOfEvent());
						}
						viewOfMainWindow.lLogs.setModel(T_Logger.filteredList);
						break;
					}
						
					case 2:
					{
						for (S_ApplicationLog x : T_Data.applicationLogs)
						{
							if (x.getFilter().getMainFilterType() == E_MainFilters.WARNING) T_Logger.filteredList.add(0, x.generateTextOfEvent());
						}
						viewOfMainWindow.lLogs.setModel(T_Logger.filteredList);
						break;
					}
						
					case 3:
					{
						for (S_ApplicationLog x : T_Data.applicationLogs)
						{
							if (x.getFilter().getMainFilterType() == E_MainFilters.ERROR) T_Logger.filteredList.add(0, x.generateTextOfEvent());
						}
						viewOfMainWindow.lLogs.setModel(T_Logger.filteredList);
						break;
					}
				}
			}
		});
	}
	
	public void addEventsTo_bAddUserFilter()
	{
		viewOfMainWindow.bAddUserFilter.addActionListener(new ActionListener()
		{		
			@Override
			public synchronized void actionPerformed(ActionEvent arg0)
			{
				String filterName = T_WindowsManager.showAddEditUserFilter(null);
				userFiltersList.addElement(filterName);
				T_Data.userFilters.add(filterName);
			}
		});
	}
	
	public void addEventsTo_bEditUserFilter()
	{
		viewOfMainWindow.bEditUserFilter.addActionListener(new ActionListener()
		{		
			@Override
			public synchronized void actionPerformed(ActionEvent arg0)
			{
				if (viewOfMainWindow.lUserFilters.getSelectedIndex() != 0)
				{
					String filterName = T_WindowsManager.showAddEditUserFilter((String) viewOfMainWindow.lUserFilters.getSelectedValue());
					if (filterName != null) 
					{
						userFiltersList.setElementAt(filterName, viewOfMainWindow.lUserFilters.getSelectedIndex());
						T_Data.userFilters.set(viewOfMainWindow.lUserFilters.getSelectedIndex() - 1, filterName);
					}
				}
			}
		});
	}
	
	public void addEventsTo_bDeleteUserFilter()
	{
		viewOfMainWindow.bDeleteUserFilter.addActionListener(new ActionListener()
		{		
			@Override
			public synchronized void actionPerformed(ActionEvent arg0)
			{
				if (viewOfMainWindow.lUserFilters.getSelectedIndex() != 0)
				{
					E_Answers answer = T_WindowsManager.showQuestion("Czy na pewno usunąć zaznaczony filtr użytkownika?");
					if (answer == E_Answers.YES)
					{
						int index = viewOfMainWindow.lUserFilters.getSelectedIndex();
						
						userFiltersList.remove(index);
						T_Data.userFilters.remove(index - 1);
					}
				}
			}
		});
	}
	
	public void addEventsTo_lUserFilters()
	{
		viewOfMainWindow.lUserFilters.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public synchronized void valueChanged(ListSelectionEvent arg0)
			{
				if (viewOfMainWindow.lUserFilters.getSelectedIndex() <= 0) viewOfMainWindow.lLogs.setModel(T_Logger.applicationLogsList);
				else
				{
					T_Logger.filteredList.clear();
					
					int selectedIndex = viewOfMainWindow.lUserFilters.getSelectedIndex();
					String userFilter = (String) viewOfMainWindow.lUserFilters.getModel().getElementAt(selectedIndex);
					
					for (S_ApplicationLog x : T_Data.applicationLogs)
					{
						
						if (x.getFilter().getUserFilterType().toLowerCase().equals(userFilter.toLowerCase())) T_Logger.filteredList.add(0, x.generateTextOfEvent());
					}
					viewOfMainWindow.lLogs.setModel(T_Logger.filteredList);
				}
			}
		});
	}
	
	public void addEventsTo_cbMouse()
	{
		viewOfMainWindow.cbMouse.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				if (viewOfMainWindow.cbMouse.isSelected()) T_Logger.startMouseLog(viewOfMainWindow.lEvents);
				else T_Logger.stopMouseLog();
			}
		});
	}
	
	public void addEventsTo_cbKeyboard()
	{
		viewOfMainWindow.cbKeyboard.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				if (viewOfMainWindow.cbKeyboard.isSelected()) T_Logger.startKeyboardLog(viewOfMainWindow.lEvents);
				else T_Logger.stopKeyboardLog();
			}
		});
	}
}
