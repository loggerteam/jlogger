package loggerteam.jlogger.windows;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.TitledBorder;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JRadioButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.AbstractListModel;
import javax.swing.DropMode;
import java.awt.Cursor;

/**
 * Widok okna "LoggerComponent".
 */
public class V_LoggerComponentView extends JFrame
{
	// ========================================
	// KONTROLKI
	// ========================================
	
	public JPanel contentPane;
	public JMenuItem miSaveLogsToTextFile;
	public JMenuItem miSaveLogsToBinaryFile;
	public JMenuItem miSaveLogsToXmlFile;
	public JMenuItem miFinish;
	public JMenuItem miAboutProgram;
	public JTabbedPane tpTabs;
	public JButton bSaveLogsToTextFile;
	public JButton bSaveLogsToBinaryFile;
	public JButton bSaveLogsToXmlFile;
	public JButton bEraseLogs;
	public JTextField tfSearch;
	public JButton bSearch;
	public JComboBox cbMainFilter;
	public JButton bAddUserFilter;
	public JButton bEditUserFilter;
	public JButton bDeleteUserFilter;
	public JList lLogs;
	public JList lUserFilters;
	public JCheckBox cbMouse;
	public JCheckBox cbKeyboard;
	public JList lEvents;
	public JLabel lLeftDiagram;
	public JLabel lRightDiagram;

	// ========================================
	// KONSTRUKTORY
	// ========================================

	/**
	 * Konstruktor domyślny.
	 */
	public V_LoggerComponentView()
	{
		setTitle("jLogger");
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/ProgramIcon.png")));
		setMinimumSize(new Dimension(620, 400));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 620, 400);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnProgram = new JMenu("jLogger");
		menuBar.add(mnProgram);
		
		miSaveLogsToTextFile = new JMenuItem("Zapisz log do pliku tekstowego");
		mnProgram.add(miSaveLogsToTextFile);
		
		miSaveLogsToBinaryFile = new JMenuItem("Zapisz log do pliku binarnego");
		mnProgram.add(miSaveLogsToBinaryFile);
		
		miSaveLogsToXmlFile = new JMenuItem("Zapisz log do pliku XML");
		mnProgram.add(miSaveLogsToXmlFile);
		
		JSeparator separator = new JSeparator();
		mnProgram.add(separator);
		
		miFinish = new JMenuItem("Zako\u0144cz");
		miFinish.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/No.png")));
		mnProgram.add(miFinish);
		
		JMenu mnPomoc = new JMenu("Pomoc");
		menuBar.add(mnPomoc);
		
		miAboutProgram = new JMenuItem("O programie");
		miAboutProgram.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/AboutProgram.png")));
		mnPomoc.add(miAboutProgram);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setPreferredSize(new Dimension(13, 36));
		contentPane.add(toolBar, BorderLayout.NORTH);
		
		bSaveLogsToTextFile = new JButton("");
		bSaveLogsToTextFile.setToolTipText("Zapisz log do pliku tekstowego");
		bSaveLogsToTextFile.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/SaveToTextFile.png")));
		toolBar.add(bSaveLogsToTextFile);
		
		bSaveLogsToBinaryFile = new JButton("");
		bSaveLogsToBinaryFile.setToolTipText("Zapisz log do pliku binarnego");
		bSaveLogsToBinaryFile.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/SaveToBinaryFile.png")));
		toolBar.add(bSaveLogsToBinaryFile);
		
		bSaveLogsToXmlFile = new JButton("");
		bSaveLogsToXmlFile.setToolTipText("Zapisz log do pliku XML");
		bSaveLogsToXmlFile.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/SaveToXmlFile.png")));
		toolBar.add(bSaveLogsToXmlFile);
		
		bEraseLogs = new JButton("");
		bEraseLogs.setToolTipText("Wyczyść");
		bEraseLogs.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/EraseLogs.png")));
		toolBar.add(bEraseLogs);
		
		tpTabs = new JTabbedPane(JTabbedPane.LEFT);
		contentPane.add(tpTabs, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tpTabs.addTab("Dziennik zdarzeń", new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/ApplicationLogs.png")), panel, null);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setPreferredSize(new Dimension(10, 65));
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Narz\u0119dzia", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_3, BorderLayout.NORTH);
		
		tfSearch = new JTextField();
		tfSearch.setText("Wyszukaj...");
		tfSearch.setPreferredSize(new Dimension(200, 24));
		panel_3.add(tfSearch);
		tfSearch.setColumns(13);
		
		bSearch = new JButton("");
		bSearch.setToolTipText("Szukaj");
		bSearch.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/Search.png")));
		bSearch.setPreferredSize(new Dimension(32, 32));
		panel_3.add(bSearch);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setPreferredSize(new Dimension(10, 0));
		panel_3.add(horizontalStrut);
		
		JLabel lblNewLabel = new JLabel("Filtr:");
		panel_3.add(lblNewLabel);
		
		cbMainFilter = new JComboBox();
		cbMainFilter.setMaximumRowCount(4);
		cbMainFilter.setModel(new DefaultComboBoxModel(new String[] {"brak", "INFO", "WARNING", "ERROR"}));
		cbMainFilter.setPreferredSize(new Dimension(100, 24));
		panel_3.add(cbMainFilter);
		
		bAddUserFilter = new JButton("");
		bAddUserFilter.setToolTipText("Dodaj filtr użytkownika");
		bAddUserFilter.setPreferredSize(new Dimension(32, 32));
		bAddUserFilter.setMinimumSize(new Dimension(20, 20));
		bAddUserFilter.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/AddUserFilter.png")));
		panel_3.add(bAddUserFilter);
		
		bEditUserFilter = new JButton("");
		bEditUserFilter.setToolTipText("Edytuj filtr użytkownika");
		bEditUserFilter.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/EditUserFilter.png")));
		bEditUserFilter.setPreferredSize(new Dimension(32, 32));
		panel_3.add(bEditUserFilter);
		
		bDeleteUserFilter = new JButton("");
		bDeleteUserFilter.setToolTipText("Usuń filtr użytkownika");
		bDeleteUserFilter.setIcon(new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/DeleteUserFilter.png")));
		bDeleteUserFilter.setPreferredSize(new Dimension(32, 32));
		panel_3.add(bDeleteUserFilter);
		
		JPanel panel_5 = new JPanel();
		panel_5.setPreferredSize(new Dimension(140, 10));
		panel_5.setBorder(new TitledBorder(null, "Filtry u\u017Cytkownika", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_5, BorderLayout.EAST);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel_5.add(scrollPane_2, BorderLayout.CENTER);
		
		lUserFilters = new JList();
		lUserFilters.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		scrollPane_2.setViewportView(lUserFilters);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Zdarzenia", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_6, BorderLayout.CENTER);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel_6.add(scrollPane_1, BorderLayout.CENTER);
		
		lLogs = new JList();
		lLogs.setModel(new AbstractListModel() {
			String[] values = new String[] {"Lore ipsum", "test", "wybrano test"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		scrollPane_1.setViewportView(lLogs);
		
		JPanel panel_1 = new JPanel();
		tpTabs.addTab("Zdarzenia urządzeń", new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/HardwareLogs.png")), panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_4.setPreferredSize(new Dimension(10, 65));
		panel_4.setBorder(new TitledBorder(null, "Filtry", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_4, BorderLayout.NORTH);
		
		cbMouse = new JCheckBox("Myszka / Touchpad");
		panel_4.add(cbMouse);
		
		cbKeyboard = new JCheckBox("Klawiatura");
		panel_4.add(cbKeyboard);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Zdarzenia", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_7, BorderLayout.CENTER);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel_7.add(scrollPane, BorderLayout.CENTER);
		
		lEvents = new JList();
		scrollPane.setViewportView(lEvents);
		
		JPanel panel_2 = new JPanel();
		tpTabs.addTab("Statystyka interfejsu", new ImageIcon(V_LoggerComponentView.class.getResource("/loggerteam/jlogger/resources/InterfaceStatistics.png")), panel_2, null);
		panel_2.setLayout(null);
		
		lLeftDiagram = new JLabel("");
		lLeftDiagram.setBounds(10, 88, 200, 200);
		panel_2.add(lLeftDiagram);
		
		JTextPane txtpnIlociWystpieniaKursora = new JTextPane();
		txtpnIlociWystpieniaKursora.setEditable(false);
		txtpnIlociWystpieniaKursora.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtpnIlociWystpieniaKursora.setBackground(SystemColor.control);
		txtpnIlociWystpieniaKursora.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtpnIlociWystpieniaKursora.setText("Ilości wystąpień kursora myszki w poszczególnych ćwiartkach ekranu");
		txtpnIlociWystpieniaKursora.setBounds(10, 11, 200, 66);
		panel_2.add(txtpnIlociWystpieniaKursora);
		
		lRightDiagram = new JLabel("");
		lRightDiagram.setBounds(226, 88, 200, 200);
		panel_2.add(lRightDiagram);
		
		JTextPane txtpnIlociWystpiePoszczeglny = new JTextPane();
		txtpnIlociWystpiePoszczeglny.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtpnIlociWystpiePoszczeglny.setEditable(false);
		txtpnIlociWystpiePoszczeglny.setText("Ilości wystąpień poszczególny typów zdarzeń");
		txtpnIlociWystpiePoszczeglny.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtpnIlociWystpiePoszczeglny.setBackground(SystemColor.menu);
		txtpnIlociWystpiePoszczeglny.setBounds(226, 11, 200, 66);
		panel_2.add(txtpnIlociWystpiePoszczeglny);
	}
}
