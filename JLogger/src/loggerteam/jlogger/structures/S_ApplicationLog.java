package loggerteam.jlogger.structures;

import java.io.Serializable;
import java.util.Date;

/**
 *klasa odpowiedzialna za logi w aplikacjach
 *@author Mateusz Mądrzecki
 *@version 1.0
 */
public class S_ApplicationLog extends A_LogItem implements Serializable {
	
// ========================================
// ATRYBUTY
// ========================================	
	
	/**
	 * Atrybut posiada nazwe klasy z które pochodzi komunikat
	 */
	private String nativeClass;
	
	/**
	 * Atrybut przechowuje tresc komunikatu
	 */
	private String text;
	
	/**
	 * Atrybut przechowuje rodzaj filtru
	 */
	private S_Filter filter;
	
// ========================================
// KONSTRUKTORY
// ========================================	
	
	/**
	 * Konstruktor posiadajacy trzy parametry
	 * @param nativeClass - nazwa klasy z której jest wywoływany
	 * @param text - tresc komunikatu
	 * @param filter - rodzaj filtru
	 */
	public S_ApplicationLog(String nativeClass, String text, S_Filter filter){
		this.nativeClass = nativeClass.split("\\$")[0];
		this.text = text;
		this.filter = filter;
		// atrybuty odziediczone z kalsy A_LogItem
		this.dateAndTime = new Date();
	}
	
// ========================================
// METODY
// ========================================
	/**
	 * Metoda odpowiada za generowanie komunikatu
	 */
	@Override
	public String generateTextOfEvent() {
		return new String(filter.getMainFilterType().toString() + ": " + dateFormat.format(dateAndTime) + " | " + timeFormat.format(dateAndTime) + " | " + nativeClass + " | " + text);
	}

// ========================================
// GETERY I SETTERY
// ========================================
	
	public String getNativeClass() {
		return nativeClass;
	}

	public String getText() {
		return text;
	}

	public S_Filter getFilter() {
		return filter;
	}

	public void setNativeClass(String nativeClass) {
		this.nativeClass = nativeClass.split("\\$")[0];
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setFilter(S_Filter filter) {
		this.filter = filter;
	}
}
