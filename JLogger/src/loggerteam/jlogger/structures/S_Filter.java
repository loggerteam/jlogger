package loggerteam.jlogger.structures;

import java.io.Serializable;

import loggerteam.jlogger.enumerations.E_MainFilters;

/**
 * Klasa odpowiedzialna za filtry.
 * @author Madrzecki Mateusz
 * @version 1.0
 */
public class S_Filter implements Serializable {
	
// ========================================
// ATRYBUTY
// ========================================
	
	/**
	 * Główny filtr.
	 */
	private E_MainFilters mainFilterType;
	
	/**
	 * Nazwa filtru użytkownika.
	 */
	private String userFilterType;
	
// ========================================
// KONSTRUKTORY
// ========================================

	public S_Filter(){
		this.mainFilterType = null;
		this.userFilterType = null;
	}
// ========================================
// GETERY I SETTERY
// ========================================
		
	public E_MainFilters getMainFilterType() {
		return mainFilterType;
	}

	public String getUserFilterType() {
		return userFilterType;
	}

	public void setMainFilterType(E_MainFilters mainFilterType) {
		this.mainFilterType = mainFilterType;
	}

	public void setUserFilterType(String userFilterType) {
		this.userFilterType = userFilterType;
	}
}
