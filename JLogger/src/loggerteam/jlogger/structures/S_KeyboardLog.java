package loggerteam.jlogger.structures;

import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.util.Date;

/**
 * Klasa reprezentująca logi przycisków z klawiatury.
 * @author Mateusz Mądrzecki
 * @version 1.0
 */
public class S_KeyboardLog extends A_LogItem implements Serializable {

// ========================================
// ATRYBUTY
// ========================================
	
	/**
	 * Pole reprezentujące wartość przyciśniętego klawisza.
	 */
	private String pressedKeyName;
	
// ========================================
// KONSTRUKTORY
// ========================================
	
	/**
	 * Konstruktor klasy przyjmujacy jako parametr wartość przyciśniętego 
	 * klawisza
	 * @param pressedKeyName
	 */
	public S_KeyboardLog(String pressedKeyName){
		this.pressedKeyName = pressedKeyName;
		//atrybuty odziedziczone z kalsy A_LogItem
		this.dateAndTime = new Date();
	}
	
// ========================================
// METODY
// ========================================
	
	/**
	 * Metoda dekoduje przyciśnięty klawisz i zamienia go na odpowiedni 
	 * komunikat. Dekodowanie odbywa się za pomocą funkcji przełączającej Switch 
	 */
	@Override
	public String generateTextOfEvent() {
		//zwracany komunikat logu
		return new String("Klawiatura: " + dateFormat.format(dateAndTime) + " | " + timeFormat.format(dateAndTime) + " | Naciśnięto klawisz: " + pressedKeyName);
	}
	
// ========================================
// GETERY I SETTERY
// ========================================
	
	public String getPressedKey() {
		return pressedKeyName;
	}

	public void setPressedKey(String pressedKey) {
		this.pressedKeyName = pressedKey;
	}
}

