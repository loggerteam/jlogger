package loggerteam.jlogger.structures;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Klasa abstrakcyjna mająca pola i metody używane w innych klasach.
 * @author Mateusz Mądrzecki
 * @version 1.0
 */
public abstract class A_LogItem implements Serializable {
	
	/**
	 * Atrybut przechowuje aktualną data i czas.
	 */
	public Date dateAndTime;
	
	/**
	 * Atrybut formatu daty.
	 */
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Atrybut formatu czasu.
	 */
	public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	
	/**
	 * Metoda tworząca odpowiednie komunikaty.
	 * @return <-- Tekst zdarzenia.
	 */
	public abstract String generateTextOfEvent();
}
