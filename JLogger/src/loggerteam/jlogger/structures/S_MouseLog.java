package loggerteam.jlogger.structures;

import java.io.Serializable;
import java.util.Date;

import loggerteam.jlogger.enumerations.E_MouseButtons;

/**
 * Klasa reprezentująca logi myszy.
 * @author Mateusz Mądrzecki
 *@version 1.0
 */
public class S_MouseLog extends A_LogItem implements Serializable {
	
// ========================================
// ATRYBUTY
// ========================================
	
	/**
	 * Atrybut reprezentuje pozycję myszki w płaszczyźnie x.
	 */
	private int xLocation;
	
	/**
	 * Atrybut reprezentuje pozycję myszki w płaszczyźnie y.
	 */
	private int yLocation;
	
	/**
	 * Atrybut reprezentuje wartość przyciśniętego przycisku myszki.
	 */
	private E_MouseButtons clickedButton;
	
// ========================================
// KONSTRUKTORY
// ========================================	
	
	/**
	 * Konstruktor parametryczny przyjmujacy parametry 
	 * @param clickedButton - status przycisków myszki
	 * @param x - pozycja myszki x
	 * @param y - pozycja myszki y
	 */
	public S_MouseLog(E_MouseButtons clickedButton, int x, int y){
		this.xLocation = x;
		this.yLocation = y;
		this.clickedButton = clickedButton;
		//atrybuty odziedziczone z kalsy A_LogItem
		this.dateAndTime = new Date();
	}
	
// ========================================
// METODY
// ========================================
	
	/**
	 * Metoda odpowiada za utworzenie odpowiedniego komunikatu logu.
	 * Jezeli nie został przycisniety żaden przycisk myszki zostaje podana tylko
	 * data godzina i pozycja myszki, jeżeli jakis przycisk myszki zosał nacisniety 
	 * zodtaje on dodany do komunikatu logu.
	 */
	@Override
	public String generateTextOfEvent() {
		//zmienna pomocnicza
		String tmpButtonName = " ";
		
		//detekcja przycisku myszki 
		if (clickedButton.equals(E_MouseButtons.NONE)) {
			return new String("Myszka: " + dateFormat.format(dateAndTime) + " | " + timeFormat.format(dateAndTime) + " | x:" + xLocation + ", y:" + yLocation);
		}
		else {
			if (clickedButton.equals(E_MouseButtons.LEFT)) tmpButtonName="LEWY";	
			else if (clickedButton.equals(E_MouseButtons.RIGHT)) tmpButtonName="PRAWY";
			else if (clickedButton.equals(E_MouseButtons.MIDDLE)) tmpButtonName="ŚRODKOWY";
			
			//komunikat logu
			return new String("Myszka: " + dateFormat.format(dateAndTime) + " | " + timeFormat.format(dateAndTime) + " | x:" + xLocation + ", y:" + yLocation +
					" | przyciśnięto przycisk: " + tmpButtonName);
		}
	}
	
// ========================================
// GETERY I SETTERY
// ========================================
	
	public int getxLocation() {
		return xLocation;
	}

	public int getyLocation() {
		return yLocation;
	}

	public E_MouseButtons getClickedButton() {
		return clickedButton;
	}

	public void setxLocation(int xLocation) {
		this.xLocation = xLocation;
	}

	public void setyLocation(int yLocation) {
		this.yLocation = yLocation;
	}

	public void setClickedButton(E_MouseButtons clickedButton) {
		this.clickedButton = clickedButton;
	}

}
