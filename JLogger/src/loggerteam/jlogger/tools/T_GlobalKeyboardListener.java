package loggerteam.jlogger.tools;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class T_GlobalKeyboardListener implements NativeKeyListener
{
	public static String pressedKey = " ";

	public T_GlobalKeyboardListener()
	{
		try
		{
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException ex)
		{
			System.err
					.println("There was a problem registering the native hook.");
			System.err.println(ex.getMessage());

			System.exit(1);
		}
		GlobalScreen.getInstance().addNativeKeyListener(this);
	}

	public void nativeKeyPressed(NativeKeyEvent e)
	{
		pressedKey = NativeKeyEvent.getKeyText(e.getKeyCode());

	}

	public void nativeKeyReleased(NativeKeyEvent e)
	{
	}

	public void nativeKeyTyped(NativeKeyEvent e)
	{

	}

}
