package loggerteam.jlogger.tools;

import java.awt.EventQueue;
import java.awt.KeyboardFocusManager;
import java.awt.MouseInfo;
import java.awt.Point;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.UIManager;

import loggerteam.jlogger.enumerations.E_MouseButtons;
import loggerteam.jlogger.structures.S_ApplicationLog;
import loggerteam.jlogger.structures.S_Filter;
import loggerteam.jlogger.structures.S_KeyboardLog;
import loggerteam.jlogger.structures.S_MouseLog;

import org.jnativehook.GlobalScreen;

public class T_Logger
{
	// ========================================
	// POLA
	// ========================================
	
	private static boolean mouseThreadWork = false;
	private static boolean keyListenerWork = false;
	public static DefaultListModel<String> eventsList = new DefaultListModel<String>();
	public static DefaultListModel<String> applicationLogsList = new DefaultListModel<String>();
	public static DefaultListModel<String> filteredList = new DefaultListModel<String>();
	
	private static T_GlobalKeyboardListener keyGlobalListener = null;
	private static T_GlobalMouseListener mouseGlobalListener= null;

	// ========================================
	// FUNKCJE
	// ========================================
	
	// TODO Potem usunąć.
//	public static void main(String[] args)
//	{
//		// Wybranie systemowego wyglądu aplikacji.
//		try 
//		{
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//		} 
//		catch (Throwable e) 
//		{
//			e.printStackTrace();
//		}
//		
//		// Uruchomienie wątku odpowiedzialnego za wyświetlanie okien.
//		EventQueue.invokeLater(new Runnable()
//		{
//			public void run()
//			{
//				try
//				{						
//					T_WindowsManager.showMainWindow();
//				} 
//				catch (Exception e)
//				{
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	public static void startLogger()
	{
		// Wybranie systemowego wyglądu aplikacji.
		try 
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} 
		catch (Throwable e) 
		{
			e.printStackTrace();
		}
		
		// Uruchomienie wątku odpowiedzialnego za wyświetlanie okien.
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{						
					T_WindowsManager.showMainWindow();
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void stopLogger()
	{
		
	}
	
	public static synchronized void startMouseLog(final JList pointer)
	{
		mouseThreadWork = true;
		
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public synchronized void run()
			{
				while (mouseThreadWork)
				{
					Point p = MouseInfo.getPointerInfo().getLocation();
					S_MouseLog mouseLog = new S_MouseLog(E_MouseButtons.NONE, p.x, p.y);
					
					if (!T_Data.mouseLogs.isEmpty())
					{
						int lastIndex = T_Data.mouseLogs.size() - 1;
						if ((mouseLog.getxLocation() == T_Data.mouseLogs.get(lastIndex).getxLocation()) && (mouseLog.getyLocation() == T_Data.mouseLogs.get(lastIndex).getyLocation())) continue;
					}
					
					T_Data.mouseLogs.add(mouseLog);
					eventsList.add(0, mouseLog.generateTextOfEvent());
					pointer.setModel(eventsList);
					
					try
					{
						Thread.sleep(1000);
					} 
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		});
		
		thread.start();
		
		// -------------------------------------------------
		
		Thread threadMat = new Thread(new Runnable()
		{
			@Override
			public synchronized void run()
			{
				mouseGlobalListener = new T_GlobalMouseListener();
	            GlobalScreen.getInstance().addNativeMouseListener(mouseGlobalListener);
	            GlobalScreen.getInstance().addNativeMouseMotionListener(mouseGlobalListener);
				while (mouseThreadWork)
				{	
					Point p = MouseInfo.getPointerInfo().getLocation();
					
					E_MouseButtons buffer = mouseGlobalListener.mouseButtonState;
					
					if (buffer == E_MouseButtons.NONE) continue;
					
					S_MouseLog mouseLog = new S_MouseLog(buffer, p.x, p.y);
					T_Data.mouseLogs.add(mouseLog);
					eventsList.add(0, mouseLog.generateTextOfEvent());
					pointer.setModel(eventsList);
					
					mouseGlobalListener.mouseButtonState = E_MouseButtons.NONE;
				}
			}
		});
		
		threadMat.start();
	}
	
	public static synchronized void stopMouseLog()
	{
		mouseThreadWork = false;
	}
	
	public static synchronized void startKeyboardLog(final JList pointer)
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
			
			keyGlobalListener = new T_GlobalKeyboardListener();
			keyListenerWork = true;
				while(keyListenerWork){
	            	if (keyGlobalListener.pressedKey != " ")
	            	{
			        	S_KeyboardLog keyboardLog = new S_KeyboardLog(keyGlobalListener.pressedKey);
						T_Data.keyboardLogs.add(keyboardLog);
						eventsList.add(0, keyboardLog.generateTextOfEvent());
						pointer.setModel(eventsList);
	            	}
	            	keyGlobalListener.pressedKey = " ";
	            	try
					{
						Thread.sleep(1);
					} 
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
	               
			}
		});
		thread.start();
	}
	
	public static synchronized void stopKeyboardLog()
	{
		keyListenerWork = false;
	}
	
	public static void addApplicationLog(String nativeClass, String text, S_Filter filter)
	{
		S_ApplicationLog applicationLog = new S_ApplicationLog(nativeClass, text, filter);
		T_Data.applicationLogs.add(applicationLog);
		applicationLogsList.add(0, applicationLog.generateTextOfEvent());
	}
}
