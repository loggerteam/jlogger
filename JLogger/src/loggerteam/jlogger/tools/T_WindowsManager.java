package loggerteam.jlogger.tools;

import loggerteam.jlogger.dialogs.L_AboutProgram;
import loggerteam.jlogger.dialogs.L_AddEditUserFilter;
import loggerteam.jlogger.dialogs.L_Information;
import loggerteam.jlogger.dialogs.L_Question;
import loggerteam.jlogger.enumerations.E_Answers;
import loggerteam.jlogger.windows.L_LoggerComponentLogic;

/**
 * Klasa do zarządzania oknami i dialogami.
 * @author Karol Wiśniewski
 */
public class T_WindowsManager
{
	// ========================================
	// POLA
	// ========================================

	// ========================================
	// FUNKCJE
	// ========================================
	
	/**
	 * Tworzy i wyświetla główne okno komponentu.
	 */
	public static void showMainWindow()
	{
		L_LoggerComponentLogic mainFrame = new L_LoggerComponentLogic();
		mainFrame.initialize();
		mainFrame.show();
	}
	
	/**
	 * Tworzy i wyświetla dialog "Information".
	 * @param description --> Treść informacji.
	 */
	public static void showInformation(String description)
	{
		L_Information information = new L_Information();
		information.initialize(description);
		information.show();
	}
	
	/**
	 * Tworzy i wyświetla dialog "Question".
	 * @param description --> Treść pytania.
	 * @return <-- Odpowiedź użytkownika.
	 */
	public static E_Answers showQuestion(String description)
	{
		L_Question question = new L_Question();
		question.initialize(description);
		question.show();
		return question.getAnswer();
	}
	
	/**
	 * Tworzy i wyświetla dialog "AboutProgram".
	 */
	public static void showAboutProgram()
	{
		L_AboutProgram aboutProgram = new L_AboutProgram();
		aboutProgram.initialize();
		aboutProgram.show();
	}
	
	/**
	 * Tworzy i wyświetla dialog "AddEditUserFilter".
	 * @param --> Nazwa filtru.
	 * @return <-- Nazwa filtru.
	 */
	public static String showAddEditUserFilter(String filterName)
	{
		L_AddEditUserFilter addEditUserFilter = new L_AddEditUserFilter();
		addEditUserFilter.initialize(filterName);
		addEditUserFilter.show();
		return addEditUserFilter.getFilterName();
	}
}
