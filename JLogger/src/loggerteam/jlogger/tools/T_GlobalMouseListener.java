package loggerteam.jlogger.tools;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;
import loggerteam.jlogger.enumerations.*;

public class T_GlobalMouseListener implements NativeMouseInputListener
{
	public static E_MouseButtons mouseButtonState = E_MouseButtons.NONE;

	public T_GlobalMouseListener()
	{

		try
		{
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException ex)
		{
			System.err
					.println("There was a problem registering the native hook.");
			System.err.println(ex.getMessage());

			System.exit(1);

		}

	}

	public void nativeMouseClicked(NativeMouseEvent e)
	{
		;
	}

	public void nativeMousePressed(NativeMouseEvent e)
	{
		switch (e.getButton())
		{
		case 1:
			mouseButtonState = E_MouseButtons.LEFT;
			break;
		case 2:
			mouseButtonState = E_MouseButtons.RIGHT;
			break;
		case 3:
			mouseButtonState = E_MouseButtons.MIDDLE;
			break;
		default:
			mouseButtonState = E_MouseButtons.NONE;
		}
	}

	public void nativeMouseReleased(NativeMouseEvent e)
	{
		;
	}

	public void nativeMouseMoved(NativeMouseEvent e)
	{
		;
	}

	public void nativeMouseDragged(NativeMouseEvent e)
	{
		;
	}

}