package loggerteam.jlogger.tools;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.thoughtworks.xstream.XStream;

import loggerteam.jlogger.structures.S_ApplicationLog;
import loggerteam.jlogger.structures.S_Filter;
import loggerteam.jlogger.structures.S_KeyboardLog;
import loggerteam.jlogger.structures.S_MouseLog;

/**
 * Klasa T_Data przechouje i zapisuje zdarzenia aplikacji, działania myszy,
 * klawiatury oraz filtrów użytkownika.
 * 
 * @author Igor Mariak, Karol Wiśniewski
 * @version 1.3
 */
public class T_Data
{
	// ------------------------------------------
	// POLA
	// ------------------------------------------
	public static ArrayList<S_ApplicationLog> applicationLogs = new ArrayList<S_ApplicationLog>();
	public static ArrayList<S_MouseLog> mouseLogs = new ArrayList<S_MouseLog>();
	public static ArrayList<S_KeyboardLog> keyboardLogs = new ArrayList<S_KeyboardLog>();
	public static ArrayList<String> userFilters = new ArrayList<String>();

	// ------------------------------------------
	// METODY
	// ------------------------------------------
	/**
	 * Zapis logów do pliku .txt Nazwa pliku :
	 * "Log Year_Month_Day Hours:Minute:Seconds.txt" Przykład : Log 2014_03_12
	 * 12:15:02.txt
	 * 
	 * @throws IOException
	 */
	public static synchronized void saveToTxtFile(String directoryPath)
			throws IOException
	{
		// Obiekty plików
		FileWriter appLogfile = null;
		FileWriter mouseLogfile = null;
		FileWriter keyLogfile = null;
		FileWriter filterLogfile = null;

		try
		{
			// Utworzenie nazwy pliku
			appLogfile = new FileWriter(directoryPath + "\\ApplicationLogs.txt");
			// Zapis treści do pliku
			for (int i = 0; i < applicationLogs.size(); i++)
			{
				appLogfile.write(applicationLogs.get(i).generateTextOfEvent()
						+ "\n");
			}
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			// Zamknięcie pliku
			appLogfile.close();
		}
		try
		{
			// Utworzenie nazwy pliku
			mouseLogfile = new FileWriter(directoryPath + "\\MouseLogs.txt");
			// Zapis treści do pliku
			for (int i = 0; i < mouseLogs.size(); i++)
			{
				mouseLogfile.write(mouseLogs.get(i).generateTextOfEvent()
						+ "\n");
			}
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			// Zamknięcie pliku
			mouseLogfile.close();
		}
		try
		{
			// Utworzenie nazwy pliku
			keyLogfile = new FileWriter(directoryPath + "\\KeyboardLogs.txt");
			// Zapis treści do pliku
			for (int i = 0; i < keyboardLogs.size(); i++)
			{
				keyLogfile.write(keyboardLogs.get(i).generateTextOfEvent()
						+ "\n");
			}
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			// Zamknięcie pliku
			keyLogfile.close();
		}
		try
		{
			// Utworzenie nazwy pliku
			filterLogfile = new FileWriter(directoryPath + "\\FilterLogs.txt");
			// Zapis treści do pliku
			for (int i = 0; i < userFilters.size(); i++)
			{
				filterLogfile.write(userFilters.get(i) + "\n");
			}
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			// Zamknięcie pliku
			filterLogfile.close();
		}
	}

	/**
	 * Zapis obiektów logów do pliku .bin
	 * 
	 * @throws IOException
	 */
	public static synchronized void saveToBinaryFile(String directoryPath)
			throws IOException
	{
		// Obiekt pliku
		ObjectOutputStream file = null;
		try
		{
			// Utworzenie nazwy pliku
			file = new ObjectOutputStream(new FileOutputStream(directoryPath
					+ "\\ApplicationLogs.bin"));
			// Zapis treści do pliku
			file.writeObject(applicationLogs);
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			file.flush();
			file.close();
		}
		try
		{
			// Utworzenie nazwy pliku
			file = new ObjectOutputStream(new FileOutputStream(directoryPath
					+ "\\MouseLogs.bin"));
			// Zapis treści do pliku
			file.writeObject(mouseLogs);
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			file.flush();
			file.close();
		}
		try
		{
			// Utworzenie nazwy pliku
			file = new ObjectOutputStream(new FileOutputStream(directoryPath
					+ "\\KeyboardLogs.bin"));
			// Zapis treści do pliku
			file.writeObject(keyboardLogs);
		} catch (Exception e)
		{
			System.err.println("Bład zapisu !!!");
		} finally
		{
			file.flush();
			file.close();
		}
		try
		{
			// Utworzenie nazwy pliku
			file = new ObjectOutputStream(new FileOutputStream(directoryPath
					+ "\\FilterLogs.bin"));
			// Zapis treści do pliku
			file.writeObject(userFilters);
		} catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} finally
		{
			file.flush();
			file.close();
		}
	}

	/**
	 * Zapisuje zdarzenia do plików z rozszerzeniem"*.xml".
	 */
	public static synchronized void saveToXmlFile(String directoryPath) throws IOException
	{
		XStream xstream = new XStream();
		xstream.alias("Zdarzenie_aplikacji", S_ApplicationLog.class);
		xstream.alias("Zdarzenie_klawiatury", S_KeyboardLog.class);
		xstream.alias("Zdarzenie_myszy", S_MouseLog.class);
		xstream.alias("Filtr_użytkownika", String.class);

		// Obiekty plików
		FileWriter appLogfile = null;
		FileWriter mouseLogfile = null;
		FileWriter keyLogfile = null;
		FileWriter filterLogfile = null;

		//
		// ZDARZENIA APLIKACJI
		//
		try
		{
			appLogfile = new FileWriter(directoryPath + "\\ApplicationLogs.xml");
			for (int i = 0; i < applicationLogs.size(); i++)
			{
				appLogfile.write(xstream.toXML(applicationLogs.get(i)) + "\n");
			}
		} 
		catch (Exception e)
		{
			System.err.println("Błąd zapisu!");
		} 
		finally
		{
			appLogfile.close();
		}
		
		//
		// ZDARZENIA MYSZY
		//
		try
		{
			mouseLogfile = new FileWriter(directoryPath + "\\MouseLogs.xml");
			for (int i = 0; i < mouseLogs.size(); i++)
			{
				mouseLogfile.write(xstream.toXML(mouseLogs.get(i)) + "\n");
			}
		} 
		catch (Exception e)
		{
			System.err.println("Błąd zapisu!");
		} 
		finally
		{
			mouseLogfile.close();
		}
		
		//
		// ZDARZENIA KLAWIATURY
		//
		try
		{
			keyLogfile = new FileWriter(directoryPath + "\\KeyboardLogs.xml");
			for (int i = 0; i < keyboardLogs.size(); i++)
			{
				keyLogfile.write(xstream.toXML(keyboardLogs.get(i))+ "\n");
			}
		} 
		catch (Exception e)
		{
			System.err.println("Błąd zapisu!");
		} 
		finally
		{
			keyLogfile.close();
		}
		
		//
		// FILTRY UŻYTKOWNIKA
		//
		try
		{
			// Utworzenie nazwy pliku
			filterLogfile = new FileWriter(directoryPath + "\\FilterLogs.xml");
			// Zapis treści do pliku
			for (int i = 0; i < userFilters.size(); i++)
			{
				filterLogfile.write(xstream.toXML(userFilters.get(i)) + "\n");
			}
		} 
		catch (Exception e)
		{
			System.err.println("Błąd zapisu !!!");
		} 
		finally
		{
			// Zamknięcie pliku
			filterLogfile.close();
		}
	}
}
