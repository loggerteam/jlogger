package loggerteam.jlogger.dialogs;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.Font;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;

/**
 * Widok dialogu "AboutProgram".
 * @author Karol Wiśniewski
 */
public class V_AboutProgram extends JDialog
{
	// ========================================
	// KONTROLKI
	// ========================================
	
	public final JPanel contentPanel = new JPanel();
	public JTextPane tpText;
	public JButton bClose;

	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public V_AboutProgram()
	{
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_AboutProgram.class.getResource("/loggerteam/jlogger/resources/ProgramIcon.png")));
		setTitle("O programie");
		setBounds(100, 100, 400, 290);
		getContentPane().setLayout(null);
		contentPanel.setBounds(74, 0, 320, 261);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		tpText = new JTextPane();
		tpText.setEditable(false);
		tpText.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tpText.setText("Aplikacja wykorzystywana jest jako komponent przy analizie działania innego programu.\r\n\r\nAutorzy:\r\nTomasz Lechman, Igor Mariak, Mateusz Mądrzecki, Karol Wiśniewski\r\n\r\nLogger Team 2014");
		tpText.setBackground(UIManager.getColor("control"));
		tpText.setBounds(10, 11, 300, 204);
		contentPanel.add(tpText);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(10, 226, 300, 35);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				bClose = new JButton("Zamknij");
				bClose.setFont(new Font("Segoe UI", Font.PLAIN, 12));
				bClose.setIcon(new ImageIcon(V_AboutProgram.class.getResource("/loggerteam/jlogger/resources/No.png")));
				bClose.setActionCommand("Cancel");
				buttonPane.add(bClose);
			}
		}
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 74, 261);
		getContentPane().add(panel);
		panel.setBackground(new Color(135, 206, 235));
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{bClose}));
	}
}
