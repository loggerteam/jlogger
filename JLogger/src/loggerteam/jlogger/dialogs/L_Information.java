package loggerteam.jlogger.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Logika dialogu "Information".
 * @author Karol Wiśniewski
 */
public class L_Information
{
	// ========================================
	// POLA
	// ========================================

	/**
	 * Widok.
	 */
	private V_Information viewOfInformation;
	
	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public L_Information()
	{
		viewOfInformation = new V_Information();
	}

	// ========================================
	// METODY
	// ========================================

	/**
	 * Inicjalizuje dialog.
	 * @param description --> Treść informacji.
	 */
	public void initialize(String description)
	{
		// Wyśrodkowanie dialogu.
		viewOfInformation.setLocationRelativeTo(null);
		
		// Dodanie zdarzeń.
		addEvents();
		
		// Ustawia treść.
		viewOfInformation.tpText.setText(description);
	}
	
	/**
	 * Wyświetla dialog.
	 */
	public void show()
	{
		viewOfInformation.setVisible(true);
	}
	
	// ========================================
	// FUNKCJE
	// ========================================

	/**
	 * Dodaje zdarzenia od kontrolek.
	 */
	private void addEvents()
	{
		addEventsTo_dialog();
		addEventsTo_bOk();
	}
	
	// ========================================
	// ZDARZENIA
	// ========================================
	
	private void addEventsTo_dialog()
	{
		viewOfInformation.addWindowListener(new WindowListener()
		{
			@Override
			public void windowOpened(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowIconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowClosing(WindowEvent arg0)
			{
				viewOfInformation.dispose();
			}
			
			@Override
			public void windowClosed(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowActivated(WindowEvent arg0)
			{
				;
			}
		});
	}
	
	private void addEventsTo_bOk()
	{
		viewOfInformation.bOk.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				viewOfInformation.dispose();
			}
		});
	}
}
