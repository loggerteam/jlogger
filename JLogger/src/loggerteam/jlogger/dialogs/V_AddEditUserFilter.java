package loggerteam.jlogger.dialogs;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.JTextField;
import java.awt.Dimension;
import javax.swing.SwingConstants;

/**
 * Widok dialogu "AddEditUserFilter".
 * @author Karol Wiśniewski
 */
public class V_AddEditUserFilter extends JDialog
{
	// ========================================
	// KONTROLKI
	// ========================================
	
	public final JPanel contentPanel = new JPanel();
	public JTextField tfName;
	public JButton bOk;
	public JButton bCancel;

	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public V_AddEditUserFilter()
	{
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_AddEditUserFilter.class.getResource("/loggerteam/jlogger/resources/ProgramIcon.png")));
		setTitle("???");
		setBounds(100, 100, 290, 120);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 284, 91);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(10, 56, 264, 35);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				bOk = new JButton("OK");
				bOk.setFont(new Font("Segoe UI", Font.PLAIN, 12));
				bOk.setIcon(new ImageIcon(V_AddEditUserFilter.class.getResource("/loggerteam/jlogger/resources/Yes.png")));
				bOk.setActionCommand("OK");
				buttonPane.add(bOk);
				getRootPane().setDefaultButton(bOk);
			}
			{
				bCancel = new JButton("Anuluj");
				bCancel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
				bCancel.setIcon(new ImageIcon(V_AddEditUserFilter.class.getResource("/loggerteam/jlogger/resources/No.png")));
				bCancel.setActionCommand("Cancel");
				buttonPane.add(bCancel);
			}
		}
		
		JLabel lblNewLabel = new JLabel("Nazwa filtru:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 15, 69, 14);
		contentPanel.add(lblNewLabel);
		
		tfName = new JTextField();
		tfName.setPreferredSize(new Dimension(6, 23));
		tfName.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		tfName.setBounds(89, 10, 185, 23);
		contentPanel.add(tfName);
		tfName.setColumns(10);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{tfName, bOk, bCancel}));
	}
}
