package loggerteam.jlogger.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import loggerteam.jlogger.enumerations.*;

/**
 * Logika dialogu "Question".
 * @author Igor Mariak, Karol Wiśniewski
 */
public class L_Question
{
	// ========================================
	// POLA
	// ========================================

	/**
	 * Widok.
	 */
	private V_Question viewOfQuestion;
	
	/**
	 * Odpowiedź użytkownika.
	 */
	private E_Answers answer;
	
	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public L_Question()
	{
		viewOfQuestion = new V_Question();
		answer = null;
	}

	// ========================================
	// METODY
	// ========================================

	/**
	 * Inicjalizuje dialog.
	 * @param description --> Treść pytania.
	 */
	public void initialize(String description)
	{
		// Wyśrodkowanie dialogu.
		viewOfQuestion.setLocationRelativeTo(null);
		
		// Dodanie zdarzeń.
		addEvents();
		
		// Ustawienie tekstu pytania.
		viewOfQuestion.tpText.setText(description);
	}
	
	/**
	 * Wyświetla dialog.
	 */
	public void show()
	{
		viewOfQuestion.setVisible(true);
	}
	
	/**
	 * Zwraca odpowiedź użytkownika.
	 * @return <-- Odpowiedź.
	 */
	public E_Answers getAnswer()
	{
		return answer;
	}
	
	// ========================================
	// FUNKCJE
	// ========================================

	/**
	 * Dodaje zdarzenia od kontrolek.
	 */
	private void addEvents()
	{
		addEventsTo_dialog();
		addEventsTo_bYes();
		addEventsTo_bNo();
	}
	
	// ========================================
	// ZDARZENIA
	// ========================================
	
	private void addEventsTo_dialog()
	{
		viewOfQuestion.addWindowListener(new WindowListener()
		{
			@Override
			public void windowOpened(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowIconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowClosing(WindowEvent arg0)
			{
				answer = E_Answers.CANCEL;
				viewOfQuestion.dispose();
			}
			
			@Override
			public void windowClosed(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowActivated(WindowEvent arg0)
			{
				;
			}
		});
	}
	
	private void addEventsTo_bYes()
	{
		viewOfQuestion.bYes.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				answer = E_Answers.YES;
				viewOfQuestion.dispose();
			}
		});
	}
	
	private void addEventsTo_bNo()
	{
		viewOfQuestion.bNo.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				answer = E_Answers.NO;
				viewOfQuestion.dispose();
			}
		});
	}
}
