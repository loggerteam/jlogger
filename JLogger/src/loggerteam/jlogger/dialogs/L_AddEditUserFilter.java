package loggerteam.jlogger.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import loggerteam.jlogger.tools.T_WindowsManager;

/**
 * Logika dialogu "AddEditUserFilter".
 * @author Karol Wiśniewski
 */
public class L_AddEditUserFilter
{
	// ========================================
	// POLA
	// ========================================

	/**
	 * Widok.
	 */
	private V_AddEditUserFilter viewOfAddEditUserFilter;
	
	/**
	 * Nazwa filtra.
	 */
	private String filterName;
	
	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public L_AddEditUserFilter()
	{
		viewOfAddEditUserFilter = new V_AddEditUserFilter();
		filterName = null;
	}

	// ========================================
	// METODY
	// ========================================

	/**
	 * Inicjalizuje dialog.
	 * @param filterName --> Nazwa filtra. Podanie wartości "null" oznacza wywołanie dialogu dodawania,
	 * natomiast podanie jakiejkolwiek innej wartości oznacza wywołanie dialogu edycji.
	 */
	public void initialize(String filterName)
	{
		// Wyśrodkowanie dialogu.
		viewOfAddEditUserFilter.setLocationRelativeTo(null);
		
		// Dodanie zdarzeń.
		addEvents();
		
		// Określenie rodzaju dialogu.
		if (filterName == null) viewOfAddEditUserFilter.setTitle("Dodaj filtr użytkownika");
		else
		{
			viewOfAddEditUserFilter.setTitle("Edytuj filtr użytkownika");
			viewOfAddEditUserFilter.tfName.setText(filterName);
		}
	}
	
	/**
	 * Wyświetla dialog.
	 */
	public void show()
	{
		viewOfAddEditUserFilter.setVisible(true);
	}
	
	/**
	 * Zwraca nazwę filtra.
	 * @return <-- Nazwa filtra.
	 */
	public String getFilterName()
	{
		return filterName;
	}
	
	// ========================================
	// FUNKCJE
	// ========================================

	/**
	 * Dodaje zdarzenia od kontrolek.
	 */
	private void addEvents()
	{
		addEventsTo_dialog();
		addEventsTo_bOk();
		addEventsTo_bCancel();
	}
	
	// ========================================
	// ZDARZENIA
	// ========================================
	
	private void addEventsTo_dialog()
	{
		viewOfAddEditUserFilter.addWindowListener(new WindowListener()
		{
			@Override
			public void windowOpened(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowIconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowClosing(WindowEvent arg0)
			{
				filterName = null;
				viewOfAddEditUserFilter.dispose();
			}
			
			@Override
			public void windowClosed(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowActivated(WindowEvent arg0)
			{
				;
			}
		});
	}
	
	private void addEventsTo_bOk()
	{
		viewOfAddEditUserFilter.bOk.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				// Sprawdzenie, czy nazwa nie jest pusta.
				if (viewOfAddEditUserFilter.tfName.getText().isEmpty()) T_WindowsManager.showInformation("Nie podano nazwy. Wprowadź tekst w pole \"Nazwa filtru\".");
				else
				{
					filterName = viewOfAddEditUserFilter.tfName.getText();
					viewOfAddEditUserFilter.dispose();
				}
			}
		});
	}
	
	private void addEventsTo_bCancel()
	{
		viewOfAddEditUserFilter.bCancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				filterName = null;
				viewOfAddEditUserFilter.dispose();
			}
		});
	}
}
