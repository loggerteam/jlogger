package loggerteam.jlogger.dialogs;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.Font;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;

/**
 * Widok dialogu "Information".
 * @author Karol Wiśniewski
 */
public class V_Information extends JDialog
{
	// ========================================
	// KONTROLKI
	// ========================================
	
	public final JPanel contentPanel = new JPanel();
	public JTextPane tpText;
	public JButton bOk;

	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public V_Information()
	{
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(V_Information.class.getResource("/loggerteam/jlogger/resources/ProgramIcon.png")));
		setTitle("Informacja");
		setBounds(100, 100, 400, 230);
		getContentPane().setLayout(null);
		contentPanel.setBounds(74, 0, 320, 201);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		tpText = new JTextPane();
		tpText.setEditable(false);
		tpText.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tpText.setText("fsdf");
		tpText.setBackground(UIManager.getColor("control"));
		tpText.setBounds(10, 11, 300, 144);
		contentPanel.add(tpText);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(10, 166, 300, 35);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				bOk = new JButton("OK");
				bOk.setFont(new Font("Segoe UI", Font.PLAIN, 12));
				bOk.setIcon(new ImageIcon(V_Information.class.getResource("/loggerteam/jlogger/resources/Yes.png")));
				bOk.setActionCommand("OK");
				buttonPane.add(bOk);
				getRootPane().setDefaultButton(bOk);
			}
		}
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 74, 201);
		getContentPane().add(panel);
		panel.setBackground(new Color(135, 206, 235));
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		{
			JLabel lblNewLabel = new JLabel("");
			panel.add(lblNewLabel);
			lblNewLabel.setIcon(new ImageIcon(V_Information.class.getResource("/loggerteam/jlogger/resources/Information.png")));
		}
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{bOk}));
	}
}
