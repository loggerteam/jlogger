package loggerteam.jlogger.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Logika dialogu "AboutProgram".
 * @author Karol Wiśniewski
 */
public class L_AboutProgram
{
	// ========================================
	// POLA
	// ========================================

	/**
	 * Widok.
	 */
	private V_AboutProgram viewOfAboutProgram;
	
	// ========================================
	// KONSTRUKTORY
	// ========================================
	
	/**
	 * Konstruktor domyślny.
	 */
	public L_AboutProgram()
	{
		viewOfAboutProgram = new V_AboutProgram();
	}

	// ========================================
	// METODY
	// ========================================

	/**
	 * Inicjalizuje dialog.
	 */
	public void initialize()
	{
		// Wyśrodkowanie dialogu.
		viewOfAboutProgram.setLocationRelativeTo(null);
		
		// Dodanie zdarzeń.
		addEvents();
	}
	
	/**
	 * Wyświetla dialog.
	 */
	public void show()
	{
		viewOfAboutProgram.setVisible(true);
	}
	
	// ========================================
	// FUNKCJE
	// ========================================

	/**
	 * Dodaje zdarzenia od kontrolek.
	 */
	private void addEvents()
	{
		addEventsTo_dialog();
		addEventsTo_bClose();
	}
	
	// ========================================
	// ZDARZENIA
	// ========================================
	
	private void addEventsTo_dialog()
	{
		viewOfAboutProgram.addWindowListener(new WindowListener()
		{
			@Override
			public void windowOpened(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowIconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeiconified(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowDeactivated(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowClosing(WindowEvent arg0)
			{
				viewOfAboutProgram.dispose();
			}
			
			@Override
			public void windowClosed(WindowEvent arg0)
			{
				;
			}
			
			@Override
			public void windowActivated(WindowEvent arg0)
			{
				;
			}
		});
	}
	
	private void addEventsTo_bClose()
	{
		viewOfAboutProgram.bClose.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				viewOfAboutProgram.dispose();
			}
		});
	}
}
