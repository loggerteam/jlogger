package loggerteam.jlogger.enumerations;

/**
 * Rodzaje filtrów.
 * @author Igor Mariak
 */
public enum E_MainFilters {
	INFO, 
	WARNING, 
	ERROR;
}
