package loggerteam.jlogger.enumerations;

/**
 * Rodzaje przycisków myszy.
 * @author Igor Mariak
 */
public enum E_MouseButtons {
	NONE,
	LEFT, 
	MIDDLE, 
	RIGHT;
}
