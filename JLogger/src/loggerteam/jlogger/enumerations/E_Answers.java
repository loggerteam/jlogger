package loggerteam.jlogger.enumerations;

/**
 * Rodzaje odpowiedzi.
 * @author Igor Mariak
 */
public enum E_Answers {
	YES, 
	NO, 
	CANCEL;
}
